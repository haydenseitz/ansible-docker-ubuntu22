PHONY: help

help:
	@echo "test		Run molecule tests"

test:
	XDG_CACHE_DIR=.cache molecule test